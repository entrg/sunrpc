typedef opaque imagedata<>;

program RESIZE_PROG {
    version RESIZE_VERS {
        imagedata RESIZE_PROC(imagedata) = 1;
    } = 1;
} = 0x20000059;