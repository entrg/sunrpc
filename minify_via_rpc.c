#include <stdlib.h>
#include <stdio.h>
#include "minifyjpeg_xdr.c"
#include "minifyjpeg_clnt.c"

CLIENT* get_minify_client(char *server){

    CLIENT *clnt;
	enum clnt_stat retval_1;
	imagedata result_1;
	imagedata resize_proc_1_arg1;

	clnt = clnt_create (server, RESIZE_PROG, RESIZE_VERS, "tcp");
	if (clnt == NULL) {
		clnt_pcreateerror (server);
		exit (1);
	}

    return clnt;
}

/*
The size of the minified file is not known before the call to the library that minimizes it,
therefore this function should allocate the right amount of memory to hold the minimized file
and return it in the last parameter to it
*/
int minify_via_rpc(CLIENT *cl, void* src_val, size_t src_len, size_t *dst_len, void **dst_val){

    enum clnt_stat retval_1;

    imagedata result_1;
	imagedata resize_proc_1_arg1;

	struct timeval tv;
    tv.tv_sec = 5;	
    tv.tv_usec = 0;
    clnt_control(cl, CLSET_TIMEOUT, &tv);
    
    //dst_val = malloc (sizeof(char * ) * src_len);

    //resize_proc_1_arg1.imagedata_val = malloc((sizeof(char)) * src_len);
    result_1.imagedata_val = malloc((sizeof(char)) * src_len);

    resize_proc_1_arg1.imagedata_val = src_val;
    resize_proc_1_arg1.imagedata_len = src_len;

    retval_1 = resize_proc_1(resize_proc_1_arg1, &result_1, cl);
	if (retval_1 != RPC_SUCCESS) {
		clnt_perror (cl, "call failed");
        return retval_1;
	}
    printf("Resampled size: = %d\n", result_1.imagedata_len);
    *dst_len = result_1.imagedata_len;
    *dst_val = result_1.imagedata_val;

    //if i free without assigning it to NULL, it also frees address space that dst_val also points where returning image data is stored in memory
    result_1.imagedata_val = NULL;

    free(result_1.imagedata_val);
    return RPC_SUCCESS;

}
