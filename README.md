# SunRPC

A remote procedure call example using SunRPC library with a single threaded server.

# Multithread

rpc_mt folder contains same example with a multithreaded server. Server implements boss/worker pattern for handling requests.
