#include "minifyjpeg.h"
#include "magickminify.h"


bool_t
resize_proc_1_svc(imagedata arg1, imagedata *result,  struct svc_req *rqstp)
{
	bool_t retval;

    magickminify_init();

	result->imagedata_val = magickminify(arg1.imagedata_val,arg1.imagedata_len, (ssize_t*)&result->imagedata_len);

    if (result->imagedata_val == NULL)
    {
        retval = 0;
    }

    else
    {
        retval = 1;
        printf("retval is not 0\n");
    }    

    magickminify_cleanup();
	return retval;
}
int resize_prog_1_freeresult (SVCXPRT *transp, xdrproc_t xdr_result, caddr_t result)
{
    	xdr_free (xdr_result, result);


	return 1;
}